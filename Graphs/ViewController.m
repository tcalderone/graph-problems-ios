//
//  ViewController.m
//  Graphs
//
//  Created by Tyler Calderone on 2/14/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "ViewController.h"
#import "Graph.h"
#import "Graph+Dijkstra.h"
#import "Vertex.h"
#import "Edge.h"
#import "Path.h"

@interface ViewController ()

@property(nonatomic, strong) Graph *graph;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  // GENERATE GRAPH
  NSLog(@"Generating graph...");
  self.graph = [[Graph alloc] init];
  // GENERATE VERTICES
  NSLog(@"Generating vertices...");
  Vertex *lax = [Vertex vertexWithName:@"LAX"];
  Vertex *sfo = [Vertex vertexWithName:@"SFO"];
  Vertex *sjc = [Vertex vertexWithName:@"SJC"];
  Vertex *sea = [Vertex vertexWithName:@"SEA"];
  Vertex *jfk = [Vertex vertexWithName:@"JFK"];
  Vertex *atl = [Vertex vertexWithName:@"ATL"];
  Vertex *mia = [Vertex vertexWithName:@"MIA"];
  // ADD VERTICES TO GRAPH
  NSLog(@"Adding vertices to graph...");
  [self.graph addVertex:lax];
  [self.graph addVertex:sfo];
  [self.graph addVertex:sjc];
  [self.graph addVertex:sea];
  [self.graph addVertex:jfk];
  [self.graph addVertex:atl];
  [self.graph addVertex:mia];
  // BUILD CONNECTIONS
  NSLog(@"Adding connections between vertices...");
  // lax
  [self.graph addConnectionBetween:lax and:sfo withLength:338];
  [self.graph addConnectionBetween:lax and:sjc withLength:308];
  [self.graph addConnectionBetween:lax and:sea withLength:955];
  [self.graph addConnectionBetween:lax and:jfk withLength:2470];
  [self.graph addConnectionBetween:lax and:atl withLength:1942];
//  [self.graph addConnectionBetween:lax and:mia withLength:2340];
  // sfo
  [self.graph addConnectionBetween:sfo and:sjc withLength:30];
  [self.graph addConnectionBetween:sfo and:sea withLength:679];
  [self.graph addConnectionBetween:sfo and:jfk withLength:2581];
  [self.graph addConnectionBetween:sfo and:atl withLength:2135];
  [self.graph addConnectionBetween:sfo and:mia withLength:2583];
  // sjc
  [self.graph addConnectionBetween:sjc and:sea withLength:697];
  [self.graph addConnectionBetween:sjc and:jfk withLength:2563];
  [self.graph addConnectionBetween:sjc and:atl withLength:2111];
  [self.graph addConnectionBetween:sjc and:mia withLength:2563];
  // sea
  [self.graph addConnectionBetween:sea and:jfk withLength:2415];
  [self.graph addConnectionBetween:sea and:atl withLength:2177];
  [self.graph addConnectionBetween:sea and:mia withLength:2722];
  // jfk
  [self.graph addConnectionBetween:jfk and:atl withLength:760];
  [self.graph addConnectionBetween:jfk and:mia withLength:1092];
  // atl
  [self.graph addConnectionBetween:atl and:mia withLength:597];

  NSLog(@"[LAX isConnected MIA] %d", [lax isConnectedTo:mia]);
  NSLog(@"[LAX -> MIA] Shortest Path: %@", [self.graph shortestPathFrom:lax to:mia]);
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
