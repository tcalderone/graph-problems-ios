//
//  Edge.h
//  Graphs
//
//  Created by Tyler Calderone on 2/14/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Vertex;
@interface Edge : NSObject

@property(nonatomic, assign, readonly) NSUInteger length;

// Class Initialization Methods
+ (instancetype)edgeWithLength:(NSUInteger)length;

// Initialization Methods
- (instancetype)initWithLength:(NSUInteger)length NS_DESIGNATED_INITIALIZER;

// Equality
- (BOOL)isEqualToEdge:(Edge *)edge;

@end
