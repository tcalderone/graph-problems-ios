//
//  Graph.h
//  Graphs
//
//  Created by Tyler Calderone on 2/15/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Vertex;
@interface Graph : NSObject

// Initialization Methods
- (instancetype)init NS_DESIGNATED_INITIALIZER;

// Vertex Management
- (BOOL)addVertex:(Vertex *)vertex;
- (NSArray *)vertices;
- (BOOL)containsVertex:(Vertex *)vertex;
- (BOOL)removeVertex:(Vertex *)vertex;

// Connection Management
- (BOOL)addConnectionBetween:(Vertex *)vA and:(Vertex *)vB withLength:(NSUInteger)length;
- (NSUInteger)lengthBetween:(Vertex *)vA and:(Vertex *)vB;
- (BOOL)removeConnectionBetween:(Vertex *)vA and:(Vertex *)vB;

@end
