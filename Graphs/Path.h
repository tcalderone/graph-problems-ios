//
//  Path.h
//  Graphs
//
//  Created by Tyler Calderone on 2/15/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Vertex;
@interface Path : NSObject <NSCopying>

@property(nonatomic, strong, readonly) Vertex *rootVertex;

// Class Initialization Methods
+ (instancetype)pathWithRoot:(Vertex *)root;

// Initialization Methods
- (instancetype)initWithRoot:(Vertex *)root NS_DESIGNATED_INITIALIZER;

// Vertex Management
- (NSArray *)connectedVertices;
- (void)addVertex:(Vertex *)vertex;
- (BOOL)containsVertex:(Vertex *)vertex;
- (void)removeVertex:(Vertex *)vertex;
- (Vertex *)tailVertex;

// Public Instance Methods
- (NSUInteger)length;

// Equality
- (BOOL)isEqualToPath:(Path *)path;

@end
