//
//  Path.m
//  Graphs
//
//  Created by Tyler Calderone on 2/15/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "Path.h"
#import "Vertex.h"

@interface Path ()

// Array<Vertex>
@property(nonatomic, strong, readonly) NSMutableArray *vertexArray;

// Internal, used for NSCopying
- (instancetype)initWithRoot:(Vertex *)root andVertices:(NSArray *)vertexArray;

@end

@implementation Path

#pragma mark - Class Initialization Methods

+ (instancetype)pathWithRoot:(Vertex *)rootVertex {
  return [[self alloc] initWithRoot:rootVertex];
}

#pragma mark - Initialization Methods

- (instancetype)initWithRoot:(Vertex *)rootVertex {
  self = [super init];
  if (self) {
    _vertexArray  = [NSMutableArray array];
    _rootVertex   = rootVertex;
  }
  return self;
}

- (instancetype)initWithRoot:(Vertex *)rootVertex andVertices:(NSArray *)vertexArray {
  self = [self initWithRoot:rootVertex];
  if (self) {
    // ensure our |_vertexArray| is mutable
    _vertexArray = [NSMutableArray arrayWithArray:vertexArray];
  }
  return self;
}

#pragma mark - Vertex Management

- (NSArray *)connectedVertices {
  // NOTE: if _mutableArray were nil, function would return nil
  // since we initialize _mutableArray within `initWithRoot:` and it is a designated initializer
  // we are safe otherwise this should be [NSArray arrayWithArray:_mutableArray]
  return [self.vertexArray copy];
}

- (void)addVertex:(Vertex *)vertex {
  [self.vertexArray addObject:vertex];
}

- (BOOL)containsVertex:(Vertex *)vertex {
  return [self.vertexArray containsObject:vertex];
}

- (void)removeVertex:(Vertex *)vertex {
  [self.vertexArray removeObject:vertex];
}

- (Vertex *)tailVertex {
  if ([self.vertexArray count] == 0) {
    return self.rootVertex;
  }

  return (Vertex *)[self.vertexArray lastObject];
}

#pragma mark - Public Instance Methods

/**
 *  Get the total length of the Path.
 *
 *  @discussion This is currently calculated at runtime. We could switch to managing the length
 *              if need be.
 *
 *  @return The total length of the Path.
 */
- (NSUInteger)length {
  NSUInteger calculatedLength = 0;

  Vertex *prevVertex = self.rootVertex;
  for (Vertex *vertex in self.vertexArray) {
    // ensure the two nodes are connected
    if (![prevVertex isConnectedTo:vertex]) {
      // return max if not connected
      return NSUIntegerMax;
    }

    calculatedLength += [prevVertex lengthTo:vertex];
    prevVertex = vertex;
  }

  return calculatedLength;
}

#pragma mark - Equality

- (BOOL)isEqualToPath:(Path *)path {
  if (!path) {
    return NO;
  } else if (![self.rootVertex isEqualToVertex:path.rootVertex]) {
    return NO;
  } else if (![self.vertexArray isEqualToArray:path.vertexArray]) {
    return NO;
  }

  return YES;

}

- (BOOL)isEqual:(id)object {
  if (self == object) {
    return YES;
  }

  if (![object isKindOfClass:[self class]]) {
    return NO;
  }

  return [self isEqualToPath:(Path *)object];
}

- (NSUInteger)hash {
  return [self.rootVertex hash] ^ [self.vertexArray hash];
}

#pragma mark - NSObject

- (NSString *)description {
  return [NSString stringWithFormat:@"<%@: %p length: %lu root: %@ vertices: %@>",
          NSStringFromClass(self.class),
          self,
          self.length,
          self.rootVertex,
          self.vertexArray];
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
  return [[[self class] alloc] initWithRoot:self.rootVertex andVertices:self.vertexArray];
}

@end
