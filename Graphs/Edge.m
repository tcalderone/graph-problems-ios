//
//  Edge.m
//  Graphs
//
//  Created by Tyler Calderone on 2/14/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "Edge.h"

@implementation Edge

#pragma mark - Class Initialization

+ (instancetype)edgeWithLength:(NSUInteger)length {
  return [[self alloc] initWithLength:length];
}

#pragma mark - Initialization

- (instancetype)initWithLength:(NSUInteger)length {
  self = [super init];
  if (self) {
    _length = length;
  }
  return self;
}

#pragma mark - Equality

- (BOOL)isEqualToEdge:(Edge *)edge {
  if (!edge) {
    return NO;
  } else if (self.length != edge.length) {
    return NO;
  }

  return YES;
}

- (BOOL)isEqual:(id)object {
  if (self == object) {
    return YES;
  }

  if (![object isKindOfClass:[self class]]) {
    return NO;
  }

  return [self isEqualToEdge:(Edge *)object];
}

- (NSUInteger)hash {
  return self.length;
}

#pragma mark - NSObject

- (NSString *)description {
  return [NSString stringWithFormat:@"<%@: %p length: %lu>",
          NSStringFromClass(self.class),
          self,
          self.length];
}

@end
