//
//  Vertex.h
//  Graphs
//
//  Created by Tyler Calderone on 2/14/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Edge;
@interface Vertex : NSObject

@property(nonatomic, copy, readonly) NSString *name;

// Class Initialization Methods
+ (instancetype)vertexWithName:(NSString *)name;

// Initialization Methods
- (instancetype)initWithName:(NSString *)name NS_DESIGNATED_INITIALIZER;

// Edge Management
- (BOOL)addEdgeTo:(Vertex *)vertex withLength:(NSUInteger)length;
- (Edge *)edgeToVertex:(Vertex *)vertex;
- (BOOL)removeEdgeTo:(Vertex *)vertex;

// Connection Querying
- (NSArray *)connectedVertices;
- (NSArray *)connectedEdges;
- (BOOL)isConnectedTo:(Vertex *)vertex;
- (NSUInteger)lengthTo:(Vertex *)vertex;

// Equality
- (BOOL)isEqualToVertex:(Vertex *)vertex;

@end
