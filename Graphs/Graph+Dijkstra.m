//
//  Graph+Dijkstra.m
//  Graphs
//
//  Created by Tyler Calderone on 2/15/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "Graph+Dijkstra.h"
#import "Vertex.h"
#import "Path.h"

@implementation Graph (Dijkstra)

- (Path *)shortestPathFrom:(Vertex *)startVertex to:(Vertex *)endVertex {
  if ([startVertex isEqualToVertex:endVertex]) {
    return [Path pathWithRoot:startVertex];
  }

  NSMutableArray *vertexArray = [NSMutableArray arrayWithArray:self.vertices];
  [vertexArray removeObject:startVertex];
  // build information data structures: Map<Vertex, Path>
  NSMapTable *vertexPathMap = [NSMapTable strongToStrongObjectsMapTable];

  // initialization
  for (Vertex *vertex in self.vertices) {
    // don't add |startVertex| to unvisited nodes
    if ([vertex isEqualToVertex:startVertex]) { continue; }

    Path *path = [Path pathWithRoot:startVertex];
    [path addVertex:vertex];
    [vertexPathMap setObject:path forKey:vertex];
  }

  // while a path to our end vertex is not found
  while ([vertexArray count]) {
    // get path with shortest distance
    Path *curPath;
    for (Vertex *vertex in vertexArray) {
      Path *path = [vertexPathMap objectForKey:vertex];
      if (!path) { continue; }
      if (!curPath || path.length < curPath.length) {
        curPath = path;
      }
    }

    // shortest path is one to |endVertex|
    if ([[curPath tailVertex] isEqualToVertex:endVertex]) {
      return curPath;
    }

    Vertex *tailVertex = curPath.tailVertex;
    for (Vertex *vertex in tailVertex.connectedVertices) {
      // check if our new length is shorter than the previous length to the same Vertex
      Path *path        = [vertexPathMap objectForKey:vertex];
      // generate distance from current vertex to the new vertex
      NSUInteger length = [tailVertex lengthTo:vertex];

      // check if this newfound path is shorter than existing path
      if (path.length == NSUIntegerMax || curPath.length + length < path.length) {
        Path *newPath = [curPath copy];
        [newPath addVertex:vertex];

        [vertexPathMap setObject:newPath forKey:vertex];
      }
    }

    [vertexArray removeObject:tailVertex];
  }

  return nil;
}

@end