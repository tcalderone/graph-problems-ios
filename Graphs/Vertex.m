//
//  Vertex.m
//  Graphs
//
//  Created by Tyler Calderone on 2/14/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "Vertex.h"
#import "Edge.h"

@interface Vertex ()

// Map<Vertex, Edge>
@property(nonatomic, strong, readonly) NSMapTable *connections;

@end

@implementation Vertex

#pragma mark - Class Initialization Methods

+ (instancetype)vertexWithName:(NSString *)name {
  return [[self alloc] initWithName:name];
}

#pragma mark - Initialization Methods

- (instancetype)initWithName:(NSString *)name {
  self = [super init];
  if (self) {
    _name         = name;
    _connections  = [NSMapTable strongToStrongObjectsMapTable];
  }
  return self;
}

#pragma mark - Edge Management

- (BOOL)addEdgeTo:(Vertex *)vertex withLength:(NSUInteger)length {
  if ([self isConnectedTo:vertex]) {
    return NO;
  }

  Edge *edge = [Edge edgeWithLength:length];
  [self.connections setObject:edge forKey:vertex];

  return YES;
}

- (Edge *)edgeToVertex:(Vertex *)vertex {
  return [self.connections objectForKey:vertex];
}

- (BOOL)removeEdgeTo:(Vertex *)vertex {
  if (![self isConnectedTo:vertex]) {
    return NO;
  }

  [self.connections removeObjectForKey:vertex];

  return YES;
}

#pragma mark - Connection Querying

- (NSArray *)connectedVertices {
  return [[self.connections keyEnumerator] allObjects];
}

- (NSArray *)connectedEdges {
  return [[self.connections objectEnumerator] allObjects];
}

- (BOOL)isConnectedTo:(Vertex *)vertex {
  return [self.connectedVertices containsObject:vertex];
}

- (NSUInteger)lengthTo:(Vertex *)vertex {
  if (![self isConnectedTo:vertex]) {
    return NSUIntegerMax;
  }

  Edge *edge = [self edgeToVertex:vertex];
  if (!edge) {
    return NSUIntegerMax;
  }

  return edge.length;
}

#pragma mark - NSObject

- (NSString *)description {
  return [NSString stringWithFormat:@"<%@: %p name: %@>",
          NSStringFromClass(self.class),
          self,
          self.name];
}

#pragma mark - Equality

- (BOOL)isEqualToVertex:(Vertex *)vertex {
  if (!vertex) {
    return NO;
  } else if (![self.name isEqualToString:vertex.name]) {
    return NO;
  } else if (![self.connections isEqual:vertex.connections]) {
    NSLog(@"Self Map: %@\nVertex Map: %@", self.connections, vertex.connections);
    return NO;
  }

  return YES;
}

- (BOOL)isEqual:(id)object {
  if (self == object) {
    return YES;
  }

  if (![object isKindOfClass:[self class]]) {
    return NO;
  }

  return [self isEqualToVertex:(Vertex *)object];
}

- (NSUInteger)hash {
  // basic hashing function
  return [self.name hash];
}

@end
