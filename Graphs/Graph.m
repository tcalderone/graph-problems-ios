//
//  Graph.m
//  Graphs
//
//  Created by Tyler Calderone on 2/15/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "Graph.h"
#import "Vertex.h"
#import "Edge.h"

@interface Graph ()

@property(nonatomic, strong, readonly) NSMutableSet *mutableVertices;

@end

@implementation Graph

#pragma mark - Initialization Methods

- (instancetype)init {
  self = [super init];
  if (self) {
    _mutableVertices = [NSMutableSet set];
  }
  return self;
}

#pragma mark - Vertex Management

- (BOOL)addVertex:(Vertex *)vertex {
  if ([self containsVertex:vertex]) {
    return NO;
  }

  [_mutableVertices addObject:vertex];
  return YES;
}

- (NSArray *)vertices {
  // NOTE: if _mutableVertices were nil, function would return nil
  // since we initialize _mutableVertices within `init`, we are safe
  // otherwise this should be [NSArray arrayWithArray:[_mutableVertices allObjects]]
  return [_mutableVertices allObjects];
}

- (BOOL)containsVertex:(Vertex *)vertex {
  return [self.vertices containsObject:vertex];
}

- (BOOL)removeVertex:(Vertex *)vertex {
  if (![self containsVertex:vertex]) {
    return NO;
  }

  [_mutableVertices removeObject:vertex];
  return YES;
}

#pragma mark - Connection Management

- (BOOL)containsBoth:(Vertex *)vA and:(Vertex *)vB {
  if (![self containsVertex:vA]) {
    return NO;
  } else if (![self containsVertex:vB]) {
    return NO;
  }

  return YES;
}

- (BOOL)addConnectionBetween:(Vertex *)vA and:(Vertex *)vB withLength:(NSUInteger)length {
  if (![self containsBoth:vA and:vB]) {
    return NO;
  }

  // add edge between both
  [vA addEdgeTo:vB withLength:length];
  [vB addEdgeTo:vA withLength:length];

  return YES;
}

- (NSUInteger)lengthBetween:(Vertex *)vA and:(Vertex *)vB {
  if (![self containsBoth:vA and:vB]) {
    return NSUIntegerMax;
  }

  Edge *edge = [vA edgeToVertex:vB];
  if (!edge) {
    return NSUIntegerMax;
  }

  return edge.length;
}

- (BOOL)removeConnectionBetween:(Vertex *)vA and:(Vertex *)vB {
  if (![self containsBoth:vA and:vB]) {
    return NO;
  }

  // remove edge between both
  [vA removeEdgeTo:vB];
  [vB removeEdgeTo:vA];

  return YES;
}

@end
