//
//  Graph+Dijkstra.h
//  Graphs
//
//  Created by Tyler Calderone on 2/15/15.
//  Copyright (c) 2015 Tyler Calderone. All rights reserved.
//

#import "Graph.h"

@class Path;
@interface Graph (Dijkstra)

/**
 * Find the shortest Path between two connected Vertex instances using Dijkstra's algorithm.
 *
 * @param startVertex Vertex to start path from.
 * @param endVertex   Vertex to end path with.
 *
 * @return A Path instance with vertices in order of travel.
 */
- (Path *)shortestPathFrom:(Vertex *)startVertex to:(Vertex *)endVertex;

@end
